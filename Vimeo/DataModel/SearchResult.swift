//
//  SearchResult.swift
//  Vimeo
//
//  Created by Dragoljub Ilic on 7/7/18.
//  Copyright © 2018 Dragoljub Ilic. All rights reserved.
//

import UIKit

protocol SearchResult {
    
    var name: String { get }
    var imageURL: URL? { get }
    
}
