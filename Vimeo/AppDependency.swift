//
//  AppDependency.swift
//  Vimeo
//
//  Created by Dragoljub Ilic on 7/7/18.
//  Copyright © 2018 Dragoljub Ilic. All rights reserved.
//

import Foundation

struct AppDependency {
    var dataManager: DataManager?
    
    init(dataManager: DataManager? = nil) {
        self.dataManager = dataManager
    }
}


protocol NeedsDependency: class {
    var dependencies: AppDependency? { get set }
}

