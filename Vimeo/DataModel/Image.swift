//
//  Image.swift
//  Vimeo
//
//  Created by Dragoljub Ilic on 7/8/18.
//  Copyright © 2018 Dragoljub Ilic. All rights reserved.
//

import Foundation

struct Image: Codable {
    let width: Int
    let height: Int
    let link: URL
}
