//
//  Channel.swift
//  Vimeo
//
//  Created by Dragoljub Ilic on 7/7/18.
//  Copyright © 2018 Dragoljub Ilic. All rights reserved.
//

import UIKit

struct Channel: Codable{
    let name: String
    private let pictures: Pictures
    var sizes: [Image] {return pictures.sizes}
}


struct channelResponse: Codable {
    let data: [Channel]
}

extension Channel: SearchResult {
    var imageURL: URL? {
        return sizes.last?.link
    }
}
