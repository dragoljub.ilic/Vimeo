//
//  DataManager.swift
//  Vimeo
//
//  Created by Dragoljub Ilic on 7/7/18.
//  Copyright © 2018 Dragoljub Ilic. All rights reserved.
//

import UIKit

final class DataManager {
    
    private var vimeo = Vimeo()
    
    private var searchResults: [String : [SearchResult]] = [:]
}

extension DataManager {
    func search(for term: String, type: Vimeo.SearchType, callback: @escaping (String, [SearchResult], DataError?) -> Void){
        
        callback(term, searchResults[term] ?? [], nil)
        
        let endpoint: Vimeo.Endpoint = .video(term: term)
        
        vimeo.call(endpoint: endpoint) {
            fileData, vimeoError in
            
            if let vimeoError = vimeoError {
                callback(term, [], .vimeoError(vimeoError))
            }
            
            guard let fileData = fileData else {
                callback(term, [], .noData)
                return
            }
            switch type {
            case .videos:
                let decoder = JSONDecoder()
                
                do {
                    let response = try decoder.decode(videoResponse.self, from: fileData)
                    let data: [SearchResult] = response.data
                    callback(term, data, nil)
                    
                } catch let error {
                    callback(term, [], .genericError(error))
                }
                
            case .channels:
                let decoder = JSONDecoder()
                
                do {
                    let response = try decoder.decode(channelResponse.self, from: fileData)
                    let data: [SearchResult] = response.data
                    callback(term, data, nil)
                    
                } catch let error {
                    callback(term, [], .genericError(error))
                }
                
            }
            //    return back to UI 
            callback(term, [], nil)
        }
    }
}


