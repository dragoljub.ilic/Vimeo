//
//  SearchCell.swift
//  Vimeo
//
//  Created by Dragoljub Ilic on 7/8/18.
//  Copyright © 2018 Dragoljub Ilic. All rights reserved.
//

import UIKit
import Kingfisher

final class SearchCell: UICollectionViewCell, NibReusableView {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var photoView: UIImageView!
    
}

extension SearchCell {
    func populate(with item:SearchResult) {
        
        label.text = item.name
        
        if let url = item.imageURL {
            photoView.kf.setImage(with: url)
        }
    }
}

