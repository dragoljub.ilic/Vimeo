//
//  ViewController.swift
//  Vimeo
//
//  Created by Dragoljub Ilic on 7/7/18.
//  Copyright © 2018 Dragoljub Ilic. All rights reserved.
//

import UIKit

class SearchController: UIViewController, NeedsDependency, StoryboardLoadable {
    
    //    External dependency
    
    var dependencies: AppDependency?
    
    //    UI
    
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var searchBar: UIVisualEffectView!
    @IBOutlet private weak var searchField: UITextField!
    @IBOutlet private weak var segmentedControl: UISegmentedControl!
    
    //    Local data model
    
    private var searchTerm: String? {
        didSet {
            if !isViewLoaded { return }
            performSearch()
        }
    }
    
    private var searchType: Vimeo.SearchType = .videos {
        didSet {
            if !isViewLoaded { return }
            performSearch()
        }
    }
    
    private var results: [SearchResult] = [] {
        didSet {
            if !isViewLoaded { return }
            collectionView.reloadData()
        }
    }
    
    private var searchWorkItem: DispatchWorkItem?
    
    //    View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = self
        //        collectionView.delegate = self
        
        let cellNib = UINib(nibName: SearchCell.reuseIdentifier, bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: SearchCell.reuseIdentifier)
        
        setupNotificationHandlers()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        collectionView.contentInset.top = searchBar.bounds.height
    }
}

private extension SearchController {
    
    func performSearch(){
        
        searchWorkItem?.cancel()
        
        let workItem = DispatchWorkItem {
            [weak self] in
            self?.search()
        }
        
        searchWorkItem = workItem
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: workItem)
    }
    
    func search() {
        
        guard let dataManager = dependencies?.dataManager else {
            fatalError("Missing data manager")
        }
        
        guard let s = searchTerm, s.count > 0 else {
            results = []
            return
        }
        
        searchWorkItem = nil
        
        dataManager.search(for: s, type: searchType) {
            [weak self] searchedTerm, arr, error in
            
            if let searchTerm = self?.searchTerm, searchTerm != searchedTerm { return }
            
            if let error = error {
                DispatchQueue.main.async {
                    let ac = UIAlertController(title: error.title, message: error.message, preferredStyle: .alert)
                    ac.addAction( UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default) )
                    self?.present(ac, animated: true, completion: nil)
                }
                return
            }
            
            if arr.count == 0 { return }
            
            DispatchQueue.main.async {
                self?.results = arr
            }
        }
    }
    
    func setupNotificationHandlers() {
        
        let nc = NotificationCenter.default
        
        nc.addObserver(forName: NSNotification.Name.UIKeyboardWillShow, object: nil, queue: .main) {
            [weak self] notification in
            self?.processKeyboardAppearance(notification)
        }
        nc.addObserver(forName: NSNotification.Name.UIKeyboardWillHide, object: nil, queue: .main) {
            [weak self] notification in
            self?.processKeyboardDisappearance(notification)
        }
        
    }
    
    func processKeyboardAppearance(_ notification: Notification) {
        guard
            let userInfo = notification.userInfo,
            let keyboardFrame = userInfo[UIKeyboardFrameEndUserInfoKey] as? CGRect
            else { return }
        
        collectionView.contentInset.bottom = keyboardFrame.height
    }
    
    func processKeyboardDisappearance(_ notification: Notification) {
        collectionView.contentInset.bottom = 0
    }
    
    @IBAction func changeSearchType(_ sender: UISegmentedControl) {
        guard let st = Vimeo.SearchType(index: sender.selectedSegmentIndex) else { return }
        searchType = st
    }
    
    @IBAction func changeSearchTerm(_ sender: UITextField) {
        searchTerm = sender.text
    }
}

extension SearchController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension SearchController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return results.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: SearchCell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchCell.reuseIdentifier, for: indexPath) as! SearchCell
        
        let res = results[indexPath.item]
        cell.populate(with: res)
        
        return cell
    }
}
