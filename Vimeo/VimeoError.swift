//
//  VimeoError.swift
//  Vimeo
//
//  Created by Dragoljub Ilic on 7/7/18.
//  Copyright © 2018 Dragoljub Ilic. All rights reserved.
//

import Foundation

enum VimeoError: Error {
    
    case urlError(URLError)
    case invalidResponse
    case noData
}
