//
//  Video.swift
//  Vimeo
//
//  Created by Dragoljub Ilic on 7/7/18.
//  Copyright © 2018 Dragoljub Ilic. All rights reserved.
//

import UIKit

struct Pictures: Codable {
    var sizes:[Image]
}

struct Video: Codable{
    let name: String
    private let pictures: Pictures
    var sizes: [Image] {return pictures.sizes}
}


struct videoResponse: Codable {
    let data: [Video]
}

extension Video: SearchResult {
    var imageURL: URL? {
        return sizes.last?.link
    }
}




    
