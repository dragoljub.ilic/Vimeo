//
//  Vimeo.swift
//  Vimeo
//
//  Created by Dragoljub Ilic on 7/7/18.
//  Copyright © 2018 Dragoljub Ilic. All rights reserved.
//

import Foundation
import SwiftyOAuth

typealias JSON = [String: Any]

final class Vimeo {
    
    typealias Callback = (Data?, VimeoError?) -> Void
    
    func call(endpoint: Endpoint, callback: @escaping Callback ){
        let apiRequest = (endpoint, callback)
        
        //    apply Authorization
        oauth(request: apiRequest)
    }
    
    private let oauthProvider = Provider.spotify(clientID: "4b4c66c41a75462fa0ae8e1e8a5813a075b8200b",
clientSecret:"vKOMYgUaejG0DQoC50J+vTLWxmbyA3HXVNsIo2lurAe11Bgaj27En1GnaXhvuIG3kF636KBDisD3xsf9Qzobyayd7gaO6V+ktK05YlRi+ziLw1lT09CmGWgZHY+vrWTA")
    
    private typealias APIRequest = (endpoint: Endpoint, callback: Callback )
    private var queuedRequests: [APIRequest] = []
    
    private var isFetchingToken = false {
        didSet {
            if isFetchingToken { return }
            processQueuedRequests()
        }
    }
}


private extension Vimeo {
    private func oauth(request: APIRequest) {
        
        // Temporaly dismised, bad response from API Authentication
        
        //        if isFetchingToken {
        //            queuedRequests.append(request)
        //            return
        //        }
        //
        //        //    is token availalbe?
        //        guard let token = oauthProvider.token else {
        //            queuedRequests.append(request)
        //
        //            fetchToken()
        //            return
        //        }
        //
        //        //    is token valid?
        //        if token.isExpired {
        //            queuedRequests.append(request)
        //
        //            refreshToken()
        //            return
        //        }
        //
        //    execute
        execute(request: request)
    }
    
    private func execute(request: APIRequest) {
        
        var urlReq = request.endpoint.urlRequest
        let callback = request.callback
        
        
        //        urlReq.addValue("Bearer \( oauthProvider.token!.accessToken )", forHTTPHeaderField: "Authorization")

        urlReq.addValue("Bearer d94f32fb7c34f5e4b2a0e2a360ddc636", forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: urlReq) {
            data, response, error in
            
            //    validate
            if let error = error {
                callback(nil, VimeoError.urlError(error as! URLError))
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                callback(nil, VimeoError.invalidResponse)
                return
            }
            
            if !(200..<300).contains(httpResponse.statusCode) {
                switch httpResponse.statusCode {
                case 401:
                    self.oauthProvider.removeToken()
                default:
                    break
                }
                callback(nil, VimeoError.invalidResponse)
                return
            }
            
            guard let data = data else {
                callback(nil, VimeoError.noData)
                return
            }
            
            callback(data, nil)
        }
        task.resume()
    }
    
    private func fetchToken() {
        if isFetchingToken { return }
        
        isFetchingToken = true
        
        oauthProvider.authorize {
            [unowned self] result in
            
            self.isFetchingToken = false
            
            switch result {
            case .success(let token):
                print(token)
            case .failure:
                break
            }
        }
    }
    
    private func refreshToken() {
        fetchToken()
    }
    
    
    func processQueuedRequests() {
        for apiReq in queuedRequests {
            oauth(request: apiReq)
        }
        queuedRequests.removeAll()
    }
    
    static let basePath: String = "https://api.vimeo.com/"
    
    static let commonHeaders: [String: String] = {
        return [
            "User-Agent": "Dragoljub v1.0",
            "Accept-Charset": "utf-8",
            "Accept-Encoding": "gzip, deflate"
        ]
    }()
}

extension Vimeo {
    
    enum SearchType: String {
        case videos
        case channels
        
        init?(index: Int) {
            switch index {
            case 0:
                self = .videos
            case 1:
                self = .channels
            default:
                return nil
            }
        }
    }
    
    enum Endpoint {
        case video(term: String)
        case channel(term: String)
        
        fileprivate var urlRequest: URLRequest {
            guard var comps = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
                fatalError("Invalid path-based URL")
            }
            comps.queryItems = queryItems
            
            guard let finalURL = comps.url else {
                fatalError("Invalid query items...(probably)")
            }
            
            var req = URLRequest(url: finalURL)
            req.httpMethod = method
            req.allHTTPHeaderFields = headers
            
            return req
        }
        
        private var headers: [String: String] {
            let h = Vimeo.commonHeaders
            
            switch self {
            case .video, .channel:
                break
            }
            return h
        }
        
        private var method: String {
            switch self {
            case .video, .channel:
                return "GET"
            }
        }
        
        private var url: URL {
            guard let fullURL = URL(string: Vimeo.basePath) else {
                fatalError("Invalid base URL")
            }
            
            switch self {
            case .video:
                return fullURL.appendingPathComponent("videos")
                
            case .channel:
                return fullURL.appendingPathComponent("channels")
            }
//            fatalError("Failed to create URL")
        }
        
        private var params: [String: Any] {
            var p: [String: Any] = [:]
            
            switch self {
            case .video(let term):
                p["query"] = term
                
            case .channel(let term):
                p["query"] = term
            }
            return p
        }
        
        private var queryItems: [URLQueryItem] {
            var arr: [URLQueryItem] = []
            
            for (key, value) in params {
                let qi = URLQueryItem(name: key, value: "\( value )")
                arr.append( qi )
            }
            return arr
        }
    }
}


