//
//  DataError.swift
//  Vimeo
//
//  Created by Dragoljub Ilic on 7/7/18.
//  Copyright © 2018 Dragoljub Ilic. All rights reserved.
//

import Foundation

enum DataError: Error {
    
    case vimeoError(VimeoError)
    case noData
    
    case notSupported
    
    case genericError(Swift.Error)
}

extension DataError {
    var title: String? {
        switch self {
        case .notSupported:
            return NSLocalizedString("Not supported yet", comment: "")
        default:
            return nil
        }
    }
    
    var message: String? {
        switch self {
        case .notSupported:
            return nil
        default:
            return NSLocalizedString("Internal error, please try again a bit later", comment: "")
        }
    }
}
